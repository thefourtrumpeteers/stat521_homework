## nhl hockey analysis

## the data is in gamlr.
## You need to first install this,
## via install.packages("gamlr")

library(gamlr) # loads Matrix as well
help(hockey) # describes the hockey data and shows an example regression

data(hockey) # load the data

# Combine the covariates all together
x <- cBind(config,team,player) # cBind binds together two sparse matrices

# build 'y': home vs away, binary response
y <- goal$homegoal

nhlreg <- gamlr(x, y,
	free=1:(ncol(config)+ncol(team)), ## free denotes unpenalized columns
	family="binomial", standardize=FALSE)

## coefficients (grab only the players)
# AICc selection
Baicc <- coef(nhlreg)[colnames(player),]


coef(nhlreg)
plot(nhlreg)

which(nhlreg$deviance == min(nhlreg$deviance))
dim(nhlreg$beta)

which(AICc(nhlreg) == min(AICc(nhlreg)))
log(nhlreg$lambda[55])

# The beta of the selected model
beta_chosen <- nhlreg$beta[ , which(AICc(nhlreg) == min(AICc(nhlreg)))]

plot(which(beta_chosen != 0))
beta_chosen_nonzero <- beta_chosen[beta_chosen != 0]
beta_chosen_nonzero

cv.nhlreg <- cv.gamlr(x, y,
                free=1:(ncol(config)+ncol(team)), ## free denotes unpenalized columns
                family="binomial", standardize=FALSE)
cv.nhlreg$seg.min
cv.nhlreg$seg.1se
log(cv.nhlreg$lambda.min)
